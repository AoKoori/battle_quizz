#!/bin/bash
tmpfile=$(mktemp)
cp appsettings.json "$tmpfile" &&
cat "$tmpfile" | jq --arg server_ip "$SERVER_IP" --arg db "$DATABASE" --arg db_user "$DB_USER" --arg db_pwd "$DB_PWD" '.ConnectionStrings.DefaultConnection |= "Server="+$server_ip+";Database="+$db+";Uid="+$db_user+";Pwd="+$db_pwd+";"' >appsettings.json &&
rm -f -- "$tmpfile"


dotnet Quizz.dll