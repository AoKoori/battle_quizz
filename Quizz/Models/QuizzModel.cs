﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quizz.Models
{
    public class Questionnaire
    {
        public int Id { get; set; }
        public string? Question { get; set; }
        public List<Proposition>? propositions { get; set; }
    }
    public class Proposition
    {
        public int Id { get; set; }
        public string? Intitule { get; set; }
        public bool IsCorrect { get; set; } 
        public Questionnaire? Questionnaire { get; set; }
    }
    public class User
    {
        public int Id { get; set; }
        public int Credits { get; set; }
        public DateTime DailyCredit { get; set; }
        public DateTime DailyCoffee { get; set; }
        public DateTime DateDernierChallenge { get; set; }
        public IdentityUser Client { get; set; }
    }
    public class Challenge
    {
        public int Id { get; set; }
        public string Createur { get; set; }
        public string Cible { get; set; }
        public string idQuestions { get; set; }
        public bool Done { get; set; }
        public DateTime DateChallenge { get; set; }
    }
    public class Score
    {
        public int Id { get; set; }
        public string user { get; set; }
        public int scoreQuestions { get; set; }
        public Challenge Challenge { get; set; }
    }
}
