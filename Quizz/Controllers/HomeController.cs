﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quizz.Data;
using Quizz.Models;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace Quizz.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var challenge = _context.Challenge.AsEnumerable().Where(s => (DateTime.Now - s.DateChallenge).TotalHours < 2 && s.Done == false).ToList();
            if (_context.User.Where(s => s.Client.Id == _userManager.GetUserId(currentUser) && s.DateDernierChallenge.Date == DateTime.Now.Date).Any())
            {
                ViewBag.dailyChallenge = true;
            }
            else
            {
                ViewBag.dailyChallenge = false;
            }

            if (_context.User.Where(s => s.Client.Id == _userManager.GetUserId(currentUser) && s.DailyCredit.Date == DateTime.Now.Date).Any())
            {
                ViewBag.dailyCredit = true;
            }
            else
            {
                ViewBag.dailyCredit = false;
            }

            if (_context.User.Where(s => s.Client.Id == _userManager.GetUserId(currentUser) && s.DailyCoffee.Date == DateTime.Now.Date && s.Credits <= 0).Any())
            {
                
                ViewBag.dailyCoffee = true;
            }
            else
            {
                ViewBag.dailyCoffee = false;
            }
            return View(challenge);
        }
        public IActionResult ValidateScore(int score, int id, string user)
        {
            var createScore = new Score
            {
                user = user,
                Challenge = _context.Challenge.Where(s => s.Id == id).FirstOrDefault(),
                scoreQuestions = score
            };
            _context.Add(createScore);
            _context.SaveChanges();
            if (_context.Score.Where(s => s.user != user).Any())
            {
                ViewBag.enemyResponse = true;    
                ViewBag.gagnant = _context.Score.OrderByDescending(s => s.scoreQuestions).Where(s => s.Challenge == createScore.Challenge).FirstOrDefault();
                var challenge = _context.Challenge.Where(s => s.Id == id).FirstOrDefault();
                challenge.Done = true;
                _context.Update(challenge);
                _context.SaveChanges();

                var user1 = _context.User.Include(s => s.Client).Where(User => User.Client.Email == challenge.Createur).FirstOrDefault();
                user1.DateDernierChallenge = DateTime.Now;
                var user2 = _context.User.Include(s => s.Client).Where(User => User.Client.Email != challenge.Createur).FirstOrDefault();

                if (user2.Client.Email == ViewBag.gagnant.user)
                {
                    user2.Credits = user2.Credits + 1;
                    user1.Credits = user1.Credits - 1;
                }
                else
                {
                    user2.Credits = user2.Credits - 1;
                    user1.Credits = user1.Credits + 1;
                }
                _context.Update(user1);
                _context.SaveChanges();

                _context.Update(user2);
                _context.SaveChanges();
            }
            else
            {
                ViewBag.enemyResponse = false;
            }
            return View(createScore);
        }

        public IActionResult Quizz(int id)
        {
            Random r = new Random();
            //var questionnaire = _context.Questionnaires.FirstOrDefault();
            var questionnaires = new List<Questionnaire>();
            var ids = _context.Challenge.Where(w => w.Id == id).Select(s => s.idQuestions).FirstOrDefault().Split(';');
            foreach (var idQuestion in ids)
            {
                questionnaires.Add(_context.Questionnaires.Include(c => c.propositions).Where(s => s.Id == Int32.Parse(idQuestion)).FirstOrDefault());
            }
            ViewBag.id = id;
            return View(questionnaires);
        }

        public IActionResult CreateChallenge(string email)
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var cible = email;
            Random r = new Random();
            var challenge = new Challenge
            {
                Cible = cible,
                Createur = _userManager.GetUserName(currentUser),
                DateChallenge = DateTime.Now,
                idQuestions = String.Join(';', _context.Questionnaires.AsEnumerable().OrderBy(x => r.Next()).Take(5).Select(s => s.Id).ToList())
            };
            _context.Add(challenge);
            _context.SaveChangesAsync();
            string to = cible;
            string from = "quizz.notification@gmail.com";
            MailMessage message = new MailMessage(from, to);
            message.Subject = "Notification Quizz";
            message.Body = @"Vous avez reçu un nouveau challenge! Rendez-vous sur le site pour faire le challenge.";
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            // Credentials are necessary if the server requires the client
            // to authenticate before it will send email on the client's behalf.
            client.Credentials = new NetworkCredential("quizz.notification@gmail.com", "AZERTY1234*");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            
            //client.UseDefaultCredentials = true;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("Index");
        }

        public IActionResult GetCredit()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var user = _context.User.Where(s => s.Client.Email == _userManager.GetUserName(currentUser)).FirstOrDefault();
            user.DailyCredit = DateTime.Now;
            user.Credits = user.Credits + 1;
            _context.Update(user);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult GetCoffee()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var user = _context.User.Where(s => s.Client.Email == _userManager.GetUserName(currentUser)).FirstOrDefault();
            user.DailyCoffee = DateTime.Now;
            user.Credits = user.Credits - 1;
            _context.Update(user);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}