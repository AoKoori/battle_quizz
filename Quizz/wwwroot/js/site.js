﻿var score = 500;
var questionRepondue = 0;
var scoreP = document.getElementById("score");
scoreP.innerHTML = score;
setInterval(function () {
    score = score - 10;
    scoreP.innerHTML = score;
}, 5000);
function checkIfTrue(isCorrect, idQuestion, idProposition) {
    var question = document.getElementById(idQuestion);
    var propositions = question.getElementsByTagName("button");
    if (isCorrect == "True") {
        document.getElementById(idProposition).className = "btn btn-success";
        score = score + 100;
        scoreP.innerHTML = score;
    }
    else {
        document.getElementById(idProposition).className = "btn btn-danger";
    }
    for (var i = 0; i < propositions.length; i++) {
        console.log(propositions.length);
        if (propositions[i].dataset.correct == "True" && propositions[i].id != idProposition) {
            propositions[i].className = "btn btn-success";
        }
        propositions[i].removeAttribute("onclick");
    }
    questionRepondue = questionRepondue + 1;
}

function isValid() {
    if (questionRepondue == 5) {
        document.getElementById("scoreFinal").value = score;
        return true;
    }
    return false;
}
