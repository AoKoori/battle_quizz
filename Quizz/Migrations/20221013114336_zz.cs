﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Quizz.Migrations
{
    public partial class zz : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Done",
                table: "Challenge",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Done",
                table: "Challenge");
        }
    }
}
