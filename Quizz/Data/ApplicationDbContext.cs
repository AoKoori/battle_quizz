﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Quizz.Models;

namespace Quizz.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<Proposition> Propositions { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Challenge> Challenge { get; set; }
        public DbSet<Score> Score { get; set; }

    }
}